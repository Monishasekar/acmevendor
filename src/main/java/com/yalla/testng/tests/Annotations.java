package com.yalla.testng.tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.yalla.selenium.api.base.SeleniumBase;

public class Annotations extends SeleniumBase {
  
  //@BeforeMethod(groups = "any")
	@Parameters ({"url","username","password"})
    @BeforeMethod
	public void beforeMethod(String url, String username, String password) {
		
	startApp("chrome", url);
	WebElement eleUserName = locateElement("id", "username");
	clearAndType(eleUserName, username);
	WebElement elePassword = locateElement("id", "password");
	clearAndType(elePassword, password);
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
  }

  //@AfterMethod(groups = "any")
  @AfterMethod
  public void afterMethod() {
	  close();
  }

}
