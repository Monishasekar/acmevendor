package com.yalla.projectDay1.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FbLike {
	
	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions op = new ChromeOptions();
        op.addArguments("--disable-notifications");
        ChromeDriver driver = new ChromeDriver(op);
		
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//login fb
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("monishasekar95@gmail.com");
		driver.findElementById("pass").sendKeys("Siddheshwaran123");
        driver.findElementByXPath("//input[@value ='Log In']").click();
        
        
		//type testleaf in home search button
		driver.findElementByXPath("//input[@class='_1frb']").sendKeys("Testleaf");
		driver.findElementByXPath("//i[@class='_585_']").click();
        
		//home search button
		Thread.sleep(3000);
		String searchTestLeaf = driver.findElementByXPath("(//div[text() = 'TestLeaf'])[1]").getText();
        
        if(searchTestLeaf.contains("TestLeaf"))
        	System.out.println(searchTestLeaf+ " is displayed");
        else
        	System.out.println(searchTestLeaf+ " is not displayed");
		
		//like
        String text = driver.findElementByXPath("(//div[text() = 'TestLeaf']/following::*[text() = 'Like'])[1]").getText();
        
        if(text.contains("Liked"))
        	System.out.println(text+ "is already liked");
        else      
        	driver.findElementByXPath("(//div[text() = 'TestLeaf']/following::*[text() = 'Like'])[1]").click();
        
        //title
        driver.findElementByXPath("(//div[text() = 'TestLeaf'])[1]").click();
        
        String text3 = driver.findElementByXPath("//span[text() = 'TestLeaf']").getText();
        
        if(text3.equals("TestLeaf"))
        	System.out.println("Title of the page is "+text3);
        else
        	System.out.println("Title not captured" +text3);
        
        String No_of_Likes = driver.findElementByXPath("//div[text() = '7,071 people like this']").getText();
        System.out.println("Number of likes: " +No_of_Likes);
        
        //driver.close();
          
	}
	 
	
}
